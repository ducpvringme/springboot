package com.ringme.entiry;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "account",catalog = "Demo1")
public class Account extends BaseEntity{
	@Column(name="AccountID")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "email",length = 50,nullable = false,unique = true)
	private String email;
	@Column(name = "userName",length = 50,nullable = false,unique = true)
	private String userName;
	@Column(name = "fullName",length = 50,nullable = false,unique = true)
	private String fullName; 
	
	@ManyToOne
	@JoinColumn(name = "departmentID", nullable = false)
	private Department department;
	
	@ManyToOne
	@JoinColumn(name = "positionID",nullable = false)
	private Position position;
	
	
	public Account() {
		super();
	}


	public Account(Long id, String email, String userName, String fullName, Department department, Position position) {
		super();
		this.id = id;
		this.email = email;
		this.userName = userName;
		this.fullName = fullName;
		this.department = department;
		this.position = position;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getFullName() {
		return fullName;
	}


	public void setFullName(String fullName) {
		this.fullName = fullName;
	}


	public Department getDepartment() {
		return department;
	}


	public void setDepartment(Department department) {
		this.department = department;
	}


	public Position getPosition() {
		return position;
	}


	public void setPosition(Position position) {
		this.position = position;
	}

}
