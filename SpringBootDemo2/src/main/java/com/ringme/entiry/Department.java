package com.ringme.entiry;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name = "department",catalog = "Demo1")
public class Department extends BaseEntity{
	@Column(name = "departmentID")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "departmentName",length = 50,nullable = false,unique = true)
	private String name;
	
	@OneToMany(mappedBy = "department",fetch = FetchType.EAGER)
	@Cascade(value = {CascadeType.REMOVE,CascadeType.SAVE_UPDATE})
	private List<Account> accounts =new ArrayList<>();
	

	public Department() {
		super();
	}

	public Department(String name, List<Account> accounts) {
		super();
		this.name = name;
		this.accounts = accounts;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}
	
}
