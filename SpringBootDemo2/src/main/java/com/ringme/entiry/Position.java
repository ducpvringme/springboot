package com.ringme.entiry;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table(name = "position",catalog = "Demo1")
public class Position extends BaseEntity{
	@Column(name = "positionID")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id; 
	
	@Column(name = "positionName",nullable = false,unique = true)
	private String name;
	
	@OneToMany(mappedBy = "position")
	private List<Account> accounts = new ArrayList<>();
	
	
	public Position() {
		super();
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Position(String name, List<Account> accounts) {
		super();
		this.name = name;
		this.accounts = accounts;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public List<Account> getAccounts() {
		return accounts;
	}


	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}
	
}
