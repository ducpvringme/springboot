package com.ringme.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.ringme.converter.AccountConverter;
import com.ringme.dto.AccountDTO;
import com.ringme.entiry.Account;
import com.ringme.entiry.Department;
import com.ringme.entiry.Position;
import com.ringme.repository.AccountRepository;
import com.ringme.repository.DepartmentRepository;
import com.ringme.repository.PositionRepository;
import com.ringme.service.implAccountService;

@Service
public class AccountService implements implAccountService {
	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private PositionRepository positionRepository;
	@Autowired
	private AccountConverter accountConverter;

	@Override
	public AccountDTO save(AccountDTO accountDTO) {
		Account account = new Account();
		if (accountDTO.getId() != null) {
			Account oldAccount = accountRepository.findOne(accountDTO.getId());
			account = accountConverter.toEntity(accountDTO, oldAccount);
		} else {
			account = accountConverter.toEntity(accountDTO);
		}
		Department department = departmentRepository.findOneByName(accountDTO.getDepartmentName());
		Position position = positionRepository.findOneByName(accountDTO.getPositionName());
		account.setDepartment(department);
		account.setPosition(position);
		account = accountRepository.save(account);
		return accountConverter.toDTO(account);
	}

	@Override
	public void delete(Long[] ids) {
		for (Long item : ids) {
			accountRepository.delete(item);
		}
	}

	@Override
	public List<AccountDTO> getAll() {
		List<AccountDTO> result = new ArrayList<>();
		List<Account> account = accountRepository.findAll();
		for (Account account3 : account) {
			AccountDTO converter = accountConverter.toDTO(account3);
			result.add(converter);
		}
		return result;
	}

	@Override
	public List<AccountDTO> getAll(Pageable pageable) {
		List<AccountDTO> result = new ArrayList<>();
		List<Account> account = accountRepository.findAll(pageable).getContent();
		for (Account account2 : account) {
			AccountDTO converter = accountConverter.toDTO(account2);
			result.add(converter);
		}
		return result;
	}

	@Override
	public Integer totalItem() {
		return (int) accountRepository.count();
	}

}
