package com.ringme.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.ringme.dto.AccountDTO;

public interface implAccountService {
	AccountDTO save(AccountDTO accountDTO);
	void delete(Long[] id);
	List<AccountDTO> getAll();
	List<AccountDTO> getAll(Pageable pageable);
	Integer totalItem();
}
