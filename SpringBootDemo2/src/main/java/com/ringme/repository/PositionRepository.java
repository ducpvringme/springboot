package com.ringme.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ringme.entiry.Position;

public interface PositionRepository extends JpaRepository<Position, Long>{
	Position findOneByName(String name);
}
