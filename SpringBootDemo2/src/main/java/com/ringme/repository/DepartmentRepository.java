package com.ringme.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ringme.entiry.Department;

public interface DepartmentRepository extends JpaRepository<Department, Long>{
	Department findOneByName(String name);
}
