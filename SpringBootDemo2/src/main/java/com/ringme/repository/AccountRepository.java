package com.ringme.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ringme.entiry.Account;

public interface AccountRepository extends JpaRepository<Account, Long>{

}
