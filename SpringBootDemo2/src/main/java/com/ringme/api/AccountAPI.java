package com.ringme.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ringme.api.out.AccountOutPut;
import com.ringme.dto.AccountDTO;
import com.ringme.service.impl.AccountService;

@RestController
public class AccountAPI {
	
	@Autowired
	private AccountService accountService;
	
	@PostMapping("/accounts")
	public AccountDTO create(@RequestBody AccountDTO accountDTO) {
		return accountService.save(accountDTO);
	}
	@PutMapping("/accounts/{id}")
	public AccountDTO update(@RequestBody AccountDTO model,@PathVariable("id")Long id) {
		model.setId(id);
		return accountService.save(model);
	}
	@GetMapping("/accounts")
	public AccountOutPut showAll(@RequestParam(value = "page",required = false)Integer page,
			                        @RequestParam(value = "limit",required = false)Integer limit) {
		AccountOutPut accountOutPut  = new AccountOutPut();
		if (page != null && limit != null) {
			accountOutPut.setPage(page);
			Pageable pageable = new PageRequest(page-1,limit);
			accountOutPut.setResult(accountService.getAll(pageable));
			accountOutPut.setTotalPage((int) Math.ceil((double)(accountService.totalItem())/limit));
		}else {
			accountOutPut.setResult(accountService.getAll());
		}
		return accountOutPut;
	}
}
