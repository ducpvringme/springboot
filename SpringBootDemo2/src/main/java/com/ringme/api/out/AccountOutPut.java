package com.ringme.api.out;

import java.util.ArrayList;
import java.util.List;

import com.ringme.dto.AccountDTO;

public class AccountOutPut {
	private int page;
	private int totalPage;
	List<AccountDTO> result = new ArrayList<>();
	
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}
	public List<AccountDTO> getResult() {
		return result;
	}
	public void setResult(List<AccountDTO> result) {
		this.result = result;
	}
}
