package com.ringme.converter;

import org.springframework.stereotype.Component;

import com.ringme.dto.AccountDTO;
import com.ringme.entiry.Account;
@Component
public class AccountConverter {
	public Account toEntity(AccountDTO dto) {
		Account account = new Account();
		account.setId(dto.getId());
		account.setCreatedBy(dto.getCreatedBy());
		account.setCreatedDate(dto.getCreatedDate());
		account.setModifiedBy(dto.getModifiedBy());
		account.setModifiedDate(dto.getModifiedDate());
		account.setEmail(dto.getEmail());
		account.setUserName(dto.getUsername());
		account.setFullName(dto.getFullname());
		return account;
	}
	public AccountDTO toDTO(Account account) {
		AccountDTO dto = new AccountDTO();
		dto.setId(account.getId());
		dto.setCreatedBy(account.getCreatedBy());
		dto.setCreatedDate(account.getCreatedDate());
		dto.setModifiedBy(account.getModifiedBy());
		dto.setModifiedDate(account.getModifiedDate());
		dto.setEmail(account.getEmail());
		dto.setUsername(account.getUserName());
		dto.setFullname(account.getFullName());
		return dto;
	}
	public Account toEntity(AccountDTO dto,Account account) {
		account.setId(dto.getId());
		account.setCreatedBy(dto.getCreatedBy());
		account.setCreatedDate(dto.getCreatedDate());
		account.setModifiedBy(dto.getModifiedBy());
		account.setModifiedDate(dto.getModifiedDate());
		account.setEmail(dto.getEmail());
		account.setUserName(dto.getUsername());
		account.setFullName(dto.getFullname());
		return account;
	}
}
