package com.ringme.dto;

public class AccountDTO extends BaseDTO<AccountDTO> {
	private Long id;
	
	private String email;
	private String username;
	private String fullname;
	private String departmentName;
	private String positionName;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}
	public AccountDTO() {
		super();
	}
	public AccountDTO(String email, String username, String fullname) {
		super();
		this.email = email;
		this.username = username;
		this.fullname = fullname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

}
