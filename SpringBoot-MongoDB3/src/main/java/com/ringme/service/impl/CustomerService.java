package com.ringme.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ringme.entitys.Customer;
import com.ringme.repository.CustomerRepository;
import com.ringme.service.ImplCustomerService;
@Service
public class CustomerService implements ImplCustomerService {
	@Autowired
	CustomerRepository customerRepository;

	@Override
	public ServiceResult findAll() {
		ServiceResult result = new ServiceResult();
		result.setData(customerRepository.findAll());
		return result;
	}

	@Override
	public ServiceResult findByID(String id) {
		ServiceResult result = new ServiceResult();
		result.setData(customerRepository.findById(id));
		return result;
	}

	@Override
	public ServiceResult save(Customer customer) {
		ServiceResult result = new ServiceResult();
		if (!customerRepository.findById(customer.getId()).isPresent()) {
			result.setData(customerRepository.save(customer));
			result.setMessage("Create success");
		}else {	
		result.setData(customerRepository.save(customer));
	    result.setMessage("Update success");
		}
		return result;
	}

	@Override
	public ServiceResult delete(String id) {
		ServiceResult result = new ServiceResult();
		Customer customer =customerRepository.findById(id).orElse(null);
		if (customer == null) {
			result.setMessage("Customer Not Found! ");
		}
		customerRepository.delete(customer);
		result.setMessage("Success");
		return result;
	}
	
}
