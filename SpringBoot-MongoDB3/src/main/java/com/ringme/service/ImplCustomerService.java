package com.ringme.service;

import com.ringme.entitys.Customer;
import com.ringme.service.impl.ServiceResult;

public interface ImplCustomerService {
	ServiceResult findAll();
	ServiceResult findByID(String id);
	ServiceResult save(Customer customer);
	ServiceResult delete(String id);
}
