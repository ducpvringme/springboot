package com.ringme;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMongoDb3Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMongoDb3Application.class, args);
	}

}
