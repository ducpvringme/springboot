package com.ringme.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ringme.entitys.Customer;
import com.ringme.service.impl.CustomerService;
import com.ringme.service.impl.ServiceResult;

@RestController
public class CustomerAPI {
	@Autowired
	private CustomerService customerService;

	@GetMapping("/customers")
	public ResponseEntity<ServiceResult> findAll() {

		return new ResponseEntity<ServiceResult>(customerService.findAll(), HttpStatus.OK);
	}

	@GetMapping("/customers/{id}")
	public ResponseEntity<ServiceResult> findByID(@PathVariable("id")String id) {

	return new ResponseEntity<ServiceResult>(customerService.findByID(id), HttpStatus.OK);
	}
	
	@PostMapping("/customers")
	public ResponseEntity<ServiceResult>create (@RequestBody Customer customer){
		
		return new ResponseEntity<ServiceResult>(customerService.save(customer),HttpStatus.OK);
		
	}
	@PutMapping("/customers/{id}")
	public ResponseEntity<ServiceResult>update(@RequestBody Customer customer,@PathVariable("id")String id){
		customer.setId(id);
		return new ResponseEntity<ServiceResult>(customerService.save(customer),HttpStatus.OK);
		
	}
	@DeleteMapping("/customers/{id}")
	public ResponseEntity<ServiceResult> deleteByID(@PathVariable("id")String id){
		return new ResponseEntity<ServiceResult>(customerService.delete(id),HttpStatus.OK);
		
	}
}
